package pis.hue2.common;

import java.net.InetAddress;

import pis.hue2.server.ClientT;

/**
 * Objekte der Klasse Teilnehmer stellen einen Teilnehmer in einem Chat dar.
 * <br>
 * Teilnehmer-Objekte besitzen immer einen Namen und eine Referenz auf den sie bedienenden Thread. Zudem koennen sie noch Informationen
 * ueber die IP-Adresse sowie den Port haben ueber die sie angemeldet wurden.
 * <br><br>
 * Klasseninvariante: <br>
 * <ul>
 * <li> String name: besteht immer aus einer Zeichenkette, die nicht mehr als dreißig Zeichen beinhalten darf. Es duerfen nur Zeichen von A-Z und
 * a-z benutzt werden, und kein ":". Darf nicht null sein. </li>
 * <li> InetAddress ip: besteht immer aus der IP-Adresse des angenommenen Clienten. Darf / Kann nicht null sein.</li>
 * <li> int port: besteht immer aus der Nummer des Ports des angenommenen Clienten. Darf / kann nicht leer sein.</li>
 * <li> ClientT clientT: ist immer die Referenz auf den Thread des angenommenen Clienten. </li>
 * </ul>
 * <br><br>
 * 
 * 
 * @author Patrick Schmidt
 * @author Anton Sedelnikow
 *
 */

public class Teilnehmer {
	
	private String name;
	private InetAddress ip;
	private int port;
	private ClientT clientT;
	
	/**
	 * Initialisiert einen Teilnehmer mit Namen und Referenz auf den bedienenden Thread.
	 * @param name ist eine Zeichenkette, besteht nur aus Zeichen A-Z, a-z und 0-9, darf kein ":" enthalten, darf nicht null sein.
	 * @param clientT ist eine Referenz auf den bedienenden Thread
	 * @throws IllegalUserException wenn Bedingungen von name nicht erfuellt werden
	 */
	
	public Teilnehmer(String name, ClientT clientT) throws IllegalUserException {			// kürzerer Konstruktor nur mit Name und einer Referenz auf den dazugehörigen Thread 				
		if (name.length() <= 30 && name.matches(".*[a-zA-Z0-9].*") && name.matches(".*[^:].*") && name != null) {								// Name darf nur 30 Zeichen lang sein 			
			this.name = name; 			
			this.clientT = clientT; 		
			} else { 			
				IllegalUserException ex = new IllegalUserException("Name ist zu lang oder beinhaltet unzulaessige Zeichen!"); 			
				throw ex; 		
			} 	
		}
	
	/**
	 * Initialisiert einen Teilnehmer mit Namen, IP-Adresse, Port und Referenz auf den bedienenden Thread.
	 * @param name ist eine Zeichenkette, besteht nur aus Zeichen A-Z, a-z und 0-9, darf kein ":" enthalten, darf nicht null sein.
	 * @param ip ist die IP-Adresse des bedienten Clienten
	 * @param port ist die Nummer des Ports des bedienten Clienten
	 * @param clientT ist eine Referenz auf den bedienenden Thread
	 * @throws IllegalUserException wenn Bedingungen von name nicht erfüllt werden
	 */
	public Teilnehmer(String name, InetAddress ip, int port, ClientT clientT) throws IllegalUserException {		// längerer Konstruktor, mit mehr Informationen und Referenz auf 
																											// den dazugehörigen Thread
		if(name.length() <= 30 && name.matches(".*[a-zA-Z0-9].*") && name.matches(".*[^:].*") && name != null) { 			
			this.name = name; 			
			this.ip = ip; 			
			this.port = port; 			
			this.clientT = clientT; 		
			} else { 			
				IllegalUserException ex = new IllegalUserException("Name ist zu lang oder beinhaltet unzulaessige Zeichen!"); 			
				throw ex; 		
			} 	
		}
	
	//-----------------------------------
	// getter Methoden für alle Attribute
	
	/**
	 * Gibt den Namen des Teilnehmers zurueck.
	 * @return name als String
	 */
	
	public String getName() {
		return name;
	}
	
	/**
	 * Gibt die IP-Adresse des Teilnehmers zurueck.
	 * @return ip als InetAddress
	 */
	
	public InetAddress getIP() {
		return ip;
	}
	
	/**
	 * Gibt die Nummer des Ports des Teilnehmers zurueck.
	 * @return port als Integer-Wert
	 */
	
	public int getPort() {
		return port;
	}
	
	/**
	 * Gibt die Referenz auf den bedienenden Thread zurueck.
	 * @return clientT als Runnable ClientT
	 */
	
	public ClientT getRef() {
		return clientT;
	}
	
	// Ende der getter-Methoden
	//--------------------------
	
	/**
	 *Gibt die gespeicherten Daten eines Teilnehmers zurueck.
	 *@return formatierte Ausgabe der Daten des Teilnehmers als String 
	 */
	
	@Override
	public String toString() {								// Der Teilnehmer kann zur Kontrolle ausgegeben werden
		if(name != null && ip == null) {
			return "Name: " + name;
		} else {
			return "Name: " + name + ", IP-Adresse: " + ip + ", Portnummer: " + port;
		}	
	}
}
