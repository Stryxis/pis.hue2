package pis.hue2.common;

/**
 * Objekte der Klasse IllegalUserException stellen Ausnahmen dar, die geworfen werden, wenn ein Teilnehmer nicht die jeweiligen Bedinungen
 * erfüllt. 
 * 
 * @author Patrick Schmidt
 * @author Anton Sedelnikow
 *
 * @see Teilnehmer
 */

public class IllegalUserException extends Exception {
	
	/**
	 * Es wird eine Exception initialisiert mit dem Konstruktor der Superklasse.
	 * @param text enthaelt die Fehlermeldung der Exception
	 */
	
	public IllegalUserException(String text) {
		super(text);
	}
}