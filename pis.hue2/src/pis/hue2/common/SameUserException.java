package pis.hue2.common;

/**
 * Objekte der Klasse SameUserException stellen Ausnahmen dar, die geworfen werden, wenn ein Teilnehmer sich bereits mit einem anderen Namen
 * oder ein anderer Teilnehmer mit dem gleichen Namen angemeldet hat. 
 * 
 * @author Patrick Schmidt
 * @author Anton Sedelnikow
 *
 * @see Teilnehmer
 */

public class SameUserException extends Exception {
	
	/**
	 * Es wird eine Exception initialisiert mit dem Konstruktor der Superklasse.
	 * @param text enthaelt die Fehlermeldung der Exception
	 */
	
	public SameUserException(String text) {
		super(text);
	}
}