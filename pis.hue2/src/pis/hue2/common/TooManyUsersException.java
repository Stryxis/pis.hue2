package pis.hue2.common;

/**
 * Objekte der Klasse TooManyUsersException stellen Ausnahmen dar, die geworfen werden, wenn sich ein Teilnehmer anmeldet, aber die Teilnehmerliste
 * bereits voll ist bzw. die maximale Benutzeranzahl uberschritten werden wuerde. 
 * 
 * @author Patrick Schmidt
 * @author Anton Sedelnikow
 *
 * @see TeilnehmerListe
 */

public class TooManyUsersException extends Exception {
	
	/**
	 * Es wird eine Exception initialisiert mit dem Konstruktor der Superklasse.
	 * @param text enthaelt die Fehlermeldung der Exception
	 */
	
	public TooManyUsersException(String text) {
		super(text);
	}
}