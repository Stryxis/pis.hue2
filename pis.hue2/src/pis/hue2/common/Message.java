package pis.hue2.common;

/**
 * Objekte der Klasse Message stellen eine Nachricht dar, mit der verschiedene Typen einer Nachricht mit einem beliebigen Inhalt 
 * verschickt / gespeichert werden können.
 * <br>
 * Message-Objekte besitzen immer einen Typ und einen Inhalt. Typ und Inhalt werden mittels eines ":" voneinander getrennt.
 * <br><br>
 * Klasseninvariante: <br>
 * <ul>
 * <li> Typ typRef: besteht immer aus einem Attribut aus dem Enum Typ.</li>
 * <li> String praeFix: besteht entweder nur aus "connect" oder "disconnect" und wird nur bei serverseitigen "confirm" Nachrichten gebraucht.</li>
 * <li> String[] content: besteht aus dem uebergeben Inhalt der Nachricht, darf auch leer oder null sein</li>
 * </ul>
 * <br><br>
 * 
 * 
 * @author Patrick Schmidt
 * @author Anton Sedelnikow
 *
 */

public class Message  {


	public enum Typ {
		disconnectConfirm, disconnect, message, nameList, refused, invalid, connectConfirm, connect;
	}
	
	private Typ typRef;
	private String praeFix;
	private String [] content;
	
	/**
	 * Initialisiert ein Message-Objekt mit einer Nachricht als String.
	 * @param message ist eine Zeichenkette einer Nachricht, muss mit einer Zeichenkette von Typ anfangen. Typ und Inhalt werden mit einem ':'
	 * getrennt. 
	 */
	
	public Message (String message) {					//1. Moeglichkeit: Nachricht über einen String definieren
		content = new String[10];						
		content = message.split(":");					// Teilung des Strings an ":" um Typ und Nachricht zu unterscheiden.
		
		for(Typ t : Typ.values()) {
			if(content[0].matches(".*" + t.toString())) {    //Hier ist die Möglichkeit einen anderen Client mit dazuzunehmen
				content[0] = t.toString();					
				break;
			}
		}
														// erste Stelle des Arrays ist der Typ, Rest Nachricht
		switch(content[0]) {							// Unterteilung der verschiednenen Typen einer Nachricht
														// Confirm-Typen sind nur serverseitig einzusetzen, deswegen auch ein Praefix
		case "connect":
			typRef = Typ.connect;
			break;
		case "connectConfirm":
			typRef = Typ.connectConfirm;
			praeFix = "connect";
			break;
		case "disconnect":
			typRef = Typ.disconnect;			
			break;
		case "disconnectConfirm":
			typRef = Typ.disconnectConfirm;
			praeFix = "disconnect";
			break;
		case "message":
			typRef = Typ.message;
			break;
		case "nameList":
			typRef  = Typ.nameList;
			break;
		case "refused": 			
			typRef = Typ.refused; 			
			break;
		default :
			typRef = Typ.invalid;
			IllegalArgumentException ex = new IllegalArgumentException("Ungueltige Nachricht!");
			throw ex; 
		}
	}
	
	/**
	 * Initilisiert ein Message-Objekt, dem ein Typ und ein inhalt uebrgeben wird.
	 * @param typRef ist ein Typ aus dem Enum Typ
	 * @param inhalt ist eine Zeichenkette
	 */
	
	public Message (Typ typRef, String inhalt) {				//2. Moeglichkeit: Nachricht über den Typ und der Nachricht als String initiieren
		this.typRef = typRef;
		content[0] = inhalt;
	}
	
	/**
	 * Gibt eine formatierte Version der Nachricht mit dem Typ und dem Inhalt als String zurueck.
	 * @return formatierte Ausgabe der Nachricht
	 */
	
	@Override
	public String toString() {
		if (typRef == Typ.connectConfirm || typRef == Typ.disconnectConfirm) {
			return praeFix + ": " + printArray();
		} else {
			return typRef.toString() + ": " + printArray();
		}
	}
	
	/**
	 * Gibt den Inhalt einer Nachricht ohne den Referenztyp zurueck.
	 * @return output als String
	 */
	
	public String getContent() {								// wahrscheinlich wichtig um die Namensliste augeben zu können
		String output = "";
		if(content[0].equals(typRef.toString())) {
			for (int i = 1; i < content.length; i++) {
				output = output + content[i].trim();
			}
			return output;
		} else {
			for(int i = 0; i < content.length; i++) {
				output = output + content[i].trim();
			}
			return output;
		}
	}
	
	/**
	 * Gibt den Typ der Nachricht wieder.
	 * @return typRef als Referenz auf einen Typ aus dem Enum Typ 
	 */
	
	public Typ getType() {
		return typRef;
	}
	
	/**
	 * Gibt das Array des Inhalt zurueck.
	 * @return help als String der das Array mit dem Inhalt der Nachricht beinhaltet
	 */
	
	private String printArray() {								// einfach nur um das Array ausgeben zu können
		String help = "";
		for (int i = 1; i < content.length; i++) {
			if(content[i] != typRef.toString()) {
				if (i < content.length -1) {
				help = help + content[i].trim() + ": ";
				} else {
					help = help + content[i].trim();
				}
			}
		}
		
		return help;
	}
}