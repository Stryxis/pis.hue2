package pis.hue2.client;

import java.net.*;
import java.io.*;
import pis.hue2.common.Message;

/**
* Klasse zur Kommunikation mit dem Socket.
* <br>
* Bietet Methoden um die Eingaben vom Nutzer und Server zu erhalten und eine Methode zum Senden an den Server.
* <br>
* <ul>
* <li>Socket client: Socket mit den Streams</li>
* <li>BufferedReader inFromUser: Für die Verarbeitung der Nachrichten vom Nutzer</li>
* <li>BufferedReader inFromServer: Für die Verarbeitung der Nachrichten vom Server</li>
* <li>PrintWriter outToServer: Zum senden  der Nachrichten an den Server</li>
* <li>Message input: Message-Objekt dass die einkommenden Streams speichert</li>
* </ul>
* 
* @author Patrick Schmidt
* @author Anton Sedelnikow
*
*/

public class Communicator implements Transceiver {
	
	private Socket client;
	private BufferedReader inFromUser;
	private BufferedReader inFromServer;
	private PrintWriter outToServer;
	private Message input;
	
	/**
	 *Konstruktor der Communicator-Klasse.
	 *<br> 
	 *<ul>
	 *<li>client:Neuer Socket  mit der lokalen IP und dem Port:12121 </li>
	 *<li>inFromUser: Stream der Konsoleneingabe </li>
	 *<li>inFromServer: Input-Stream des Sockets</li>
	 *<li>outToServer: Output-Stream des Sockets</li>
	 *</ul>
	 *
	 * @throws IOException
	 */
	
	public Communicator() throws IOException {
		client = new Socket("localhost", 12121);
		inFromUser = new BufferedReader(new InputStreamReader(System.in));
		inFromServer = new BufferedReader(new InputStreamReader(client.getInputStream()));
		outToServer = new PrintWriter(client.getOutputStream(),true);
	}
	
	/**
	 * Methode um eine Nachricht an den OutputStream des Sockets zu senden.
	 * 
	 * @param Message msg: Message Objekt zum speichern der zu versendenden Message
	 * 
	 * @throws IOException
	 */

	@Override
	public void writeOut(Message msg) throws IOException {
		
		outToServer.println(msg.toString().trim());			// Nachricht wird an den Server geschickt. Momentan fällt mir keine Notwendigkeit einer
															// Unterteilung ein
		
	}
	
	/**
	 * Methode zum einlesen der Befehle vom Server.
	 * <br>
	 * Es wird überprüft ob ein disconnectConfirm versendet wurde. Ist das der Fall werden
	 * die Verbindungen geschlossen
	 * 
	 * @param String inLine: String zum speichern der eingehenden Befehle.
	 */
	
	@Override
	public Message readInServer() throws IOException {
		String inLine;
		while((inLine = inFromServer.readLine()) != null) {
			input = new Message(inLine.trim());
			switch(input.getType()) {				// Unterscheidung des Typs in die beiden wichtigen beim Empfangen vom Server
			case disconnectConfirm:					// Wichtig nur: wenn disconnectConfirm, dann alles schließen und Verbindung abbrechen
				inFromUser.close();					// sonst gibt er einfach das eingelesene zurück
				inFromServer.close();
				outToServer.close();
				client.close();
				return input;
			default:
				return input;
			}
		}
		return input;
		
	}

	/** 
	 * Methode zum Einlesen der Eingaben des Clients.
	 */
	
	@Override
	public Message readInUser() throws IOException {				// Einlesen der Eingaben des Clients
		String inLine;
		try { 			
			while((inLine = inFromUser.readLine()) != null) { 			
				input = new Message(inLine.trim()); 			
				break; 		
				} 		
			//return input; 		
			} catch (IllegalArgumentException ex) { 			
				System.out.println("Invalid Message!"); 			
				input = new Message("disconnect:"); 		
			} 		
		return input;
	}
}


