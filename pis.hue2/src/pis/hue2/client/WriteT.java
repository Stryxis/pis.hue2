package pis.hue2.client;

import java.io.IOException;

import pis.hue2.common.Message;

public class WriteT implements Runnable {

	private boolean open;
	private Transceiver trans;
	
	public WriteT(Transceiver trans) {
		this.trans = trans;
	}
	
	@Override
	public void run() {
		// while(pressed){
		while (open) {
			try {

				Message msg = trans.readInUser();					//Fuer die Konsoleneingabe
				trans.writeOut(msg);
				if (msg.getType() == Message.Typ.disconnect) {
					Thread.currentThread().interrupt();
				}

			} catch (IOException ex) {
				ex.printStackTrace();
				Thread.currentThread().interrupt();
			}
		}

	}

}
