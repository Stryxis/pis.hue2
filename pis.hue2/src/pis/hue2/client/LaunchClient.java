package pis.hue2.client;

import java.io.*;

import javax.swing.SwingUtilities;


/**
 * Klasse zum Starten des Clients.
 * Neuer ClientGUI wird erstellt.
 * 
 * @author Patrick Schmidt
 * @author Anton Sedelnikow
 *
 */

public class LaunchClient {
	
	private static Transceiver trans;
	
	public static void main(String[] args) {
		
		try {
			trans = new Communicator();							// Klasse in der die Methoden zum lesen und schreiben sind

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Output gui = new ClientGUI(trans);
			}
		});

	}

}
