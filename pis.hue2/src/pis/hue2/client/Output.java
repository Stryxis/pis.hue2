package pis.hue2.client;

import java.io.IOException;

public interface Output {
	
	public void printNameList(String s) throws IOException;
	public void printMessage() throws IOException;
	public void printTextIncoming(String txt);
	public void clearList();
}
