package pis.hue2.client;

import java.io.IOException;

import pis.hue2.common.Message;

public class ReadT implements Runnable {

	private boolean open;
	private Message print[];
	private Output output;
	private Transceiver trans;
	
	public ReadT(Output output, Transceiver trans) {
		this.output = output;
		this.trans = trans;
		open = true;
		print = new Message[10];
	}
	
	@Override
	public void run() {
		while (open) {

			try {

				for (int i = 0; i < print.length; i++) {
					print[i] = trans.readInServer();
					System.out.println(print[i].toString());					//Stream wird in der Konsole ausgegeben
					
					if(print[i].getType() == Message.Typ.message){										//Wenn der Befehl message: empfangen wird wird die Befehlsbezeichnung
						output.printTextIncoming(print[i].toString().trim().replaceAll("message:", "")+"\n");	//entfernt und die Nachricht im Chatfenster angezeigt.
					}else if (print[i].getType() == Message.Typ.nameList) {								//Wenn der Befehl nameList: empfangen wird, wird die Namensliste
																										//gespeichert und an die printNameList Methode uebergeben
						//String s = print[i].toString();
						output.printNameList(print[i].toString());

						break;
					}else if (print[i].getType() == Message.Typ.disconnect) {							//Beim empfangen des Befehls disconnect:
						//feld_user.setText(null);														//Werden alle Textinhalte geloescht und eine
						//feld_lesen.setText(null);														// Infonachricht angezeigt
						output.printTextIncoming("Verbindung getrennt!\n");
						//feld_schreiben.setText(null);
						output.clearList();
						open = false;
						break;
					}else if(print[i].getType()== Message.Typ.refused){
						//feld_lesen.setText(null);
						output.printTextIncoming("Nutzername wird bereits verwendet\n");
					}
					
					i = 0;
				}

			} catch (IOException e) {
				e.printStackTrace();
				Thread.currentThread().interrupt();
			}

		}
	}
}

