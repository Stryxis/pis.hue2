package pis.hue2.client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;

import javax.swing.*;
import pis.hue2.common.Message;

/**
 * Klasse um die grafische Oberflaeche zu erstellen.
 * <br>
 *  Es werden Labels, TextAreas, Textfields, Listen und Buttons angelegt,
 * welche im Anschluss initialisiert werden.
 * Um die Funktionalitaet zu erhoehen wurden ActionListener und WindowListener angelegt, welche beim klicken auf den Button reagieren
 * <br>
 * 
 * <ul>
 * <li> Transceiver trans: Interface welches die Streams des Sockets bereitstellt.</li>
 * <li> boolean open: Boolean um die Threads aktiv zu halten.</li>
 * <li> Message[] print: Message Array fuer den eingehenden Stream.</li> 
 * <li> boolean pressed: Boolean fuer den Versand einer Nachricht. </li>
 * </ul>
 * 
 * 
 * 
 * 
 * @author Patrick Schmidt
 * @author Anton Sedelnikow
 *
 */

public class ClientGUI extends JFrame implements Output, ActionListener, WindowListener {

	// Komponenten
	
	private Runnable read;
	private Thread readT;
	private Runnable write;
	private Thread writeT;
	private static Transceiver trans;
	private boolean pressed = false;

	// Labels
	private JLabel user;
	
	// Textfields
	private JTextField feld_user;
	private JTextField feld_schreiben;

	// Textarea
	private JTextArea feld_lesen;
	private JTextArea feld_nutzer;

	

	// Buttons
	private JButton connect;
	private JButton disconnect;
	private JButton send;

	private JScrollPane scroll_lesen;


	// Container
	private JPanel top;
	private JPanel bottom;

	// Lists
	private JList<String> benutzer;
	private DefaultListModel<String> listeDefault;
	
	/**
	 * Konstruktor indem Titel, Groesse und die Standard Methode zum schliessen definiert wird.
	 * Anschliessend werden die Methoden zur Initialisierung der Komponenten und Listener aufgerufen.
	 */

	public ClientGUI(Transceiver trans) {
		ClientGUI.trans = trans;

		setTitle("Client");
		setSize(800, 200);

		setDefaultCloseOperation(EXIT_ON_CLOSE);
			
		this.initComp();
		this.initListen();

		setVisible(true);

	}

	/**
	 * Methode zur Initialisierung der Komponenten und zur Erstellung des Frames.
	 * Es wird ein Standard BorderLayout verwendet.
	 * 
	 * Label und Feld fuer den Nutzernamen
	 * wurden gemeinsam mit den Buttons zu einem Container zusammengefasst und bilden
	 * den Page_Start Container des Layouts.
	 * 
	 * Der Center Container wird mit einer TextArea fuer die eingehenden Nachrichten und
	 * einer Liste fuer die verbundenen Nutzer ausgefuellt.
	 * 
	 * Im Page_End Container des Layouts befindet sich das TextField fuer die Eingabe.
	 * 
	 */
	
	private void initComp() {

		// Labels
		this.user = new JLabel("Benutzername");
		
		
		// Textfield
		this.feld_user = new JTextField(15);

		this.feld_schreiben = new JTextField(50);

		// Textarea
		this.feld_lesen = new JTextArea();
		this.feld_lesen.setEditable(false);

		this.feld_nutzer = new JTextArea();

		new JScrollPane(feld_nutzer);
		this.scroll_lesen = new JScrollPane(feld_lesen);
		
		
		
		// Buttons
		this.connect = new JButton("Verbinden");

		this.disconnect = new JButton("Trennen");

		this.send = new JButton("Senden");

		// Lists
		this.listeDefault = new DefaultListModel<String>();
		this.benutzer = new JList<String>(listeDefault);
		benutzer.setPrototypeCellValue("xxxxxxxxxxxxxxxxxxxx");

		// Container
		this.top = new JPanel();
		this.top.add(user);
		this.top.add(feld_user);
		this.top.add(connect);
		this.top.add(disconnect);
		this.top.add(send);
		
		this.bottom=new JPanel();
		this.bottom.add(feld_schreiben);

		
		// Frame
		this.getContentPane().add(top, "North");
		this.getContentPane().add(scroll_lesen, "Center");
		this.getContentPane().add(feld_schreiben, "South");
		
		this.getContentPane().add(new JScrollPane(benutzer), "East");
		

	}

	/**
	 * Methode zur Initialisierung der Listener.
	 * Der Verbinden-, Trennen- und Senden-Button wurden mit ActionCommands und -Listeners versehen
	 * um auf Knopfdruck zu reagieren.
	 * Die Felder fuer den Nutzernamen und die Eingaben wurden ebenfalls mit Listenern versehen
	 * um auf die Entertaste zu reagieren.
	 *
	 * Zuzueglich wurde noch ein WindowListener angelegt um beim Schliessen des Fensters
	 * eine disconnect: message zu senden.
	 */
	
	private void initListen() {
		this.connect.setActionCommand("connect");
		this.connect.addActionListener(this);
		this.feld_user.setActionCommand("connect");
		this.feld_user.addActionListener(this);
		
		
		this.disconnect.setActionCommand("disconnect");
		this.disconnect.addActionListener(this);

		this.send.setActionCommand("send");
		this.send.addActionListener(this);
		this.feld_schreiben.setActionCommand("send");
		this.feld_schreiben.addActionListener(this);
	
		addWindowListener(this);
		
		
	}

	/**
	 * Methoden des ActionListeners.
	 * <br>
	 * Beim druecken auf die Verbinden-Taste wird eine connect:-Message gesendet mit
	 * dem Inhalt des Benutzernamen-Feldes. Anschliessend werden die Threads zum Lesen und Schreiben gestarten.
	 * Der read-Thread reagiert auf die verschiedenen Befehle der Message-Klasse.
	 * <br>
	 * Beim druecken auf die Trennen-Taste wird eine disconnect:-Message gesendet.
	 * <br>
	 * Beim druecken auf die Senden-Taste wird die printMessage Methode aufgerufen um eine Nachricht zu versenden
	 * 
	 * @param String command:String in dem die oben gesetzten ActionCommands gespeichert und verglichen werden.
	 * 
	 * @throws IOException
	 */
	
	@Override
	public void actionPerformed(ActionEvent e) {

		String command = e.getActionCommand();
		if (command.equals("connect")) {													//Beim druecke auf den Verbinden-Button
			Message usr = new Message("connect:" + feld_user.getText());					//Es wird ein Message-Objekt mit dem connect: Befehl und dem Inhalt des Benutzernamen-Feldes
			printTextIncoming("Sie sind verbunden als: "+feld_user.getText()+"\n");			//Nachricht wird im Chatfenster angezeigt
			
			try {
				trans.writeOut(usr);														//Message rausschicken
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			read = new ReadT(this, trans);
			readT = new Thread(read);
			
			write = new WriteT(trans);
			writeT = new Thread(write);
			
			writeT.start();
			readT.start();

		} else if (command.equals("disconnect")) {
			Message disconnect = new Message("disconnect:");
			try {
				trans.writeOut(disconnect);
			} catch (IOException e1) {
				e1.printStackTrace();
			}

		} else if (command.equals("send")) {					//Beim druecken auf die Senden Taste wird die printMessage() Methode aufgerufen

			pressed = true;
			while (pressed) {
				try {
					printMessage();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}

		}

	}

	/**
	 * Methode zur Anzeige der mit dem Server verbundenen Nutzer.
	 * Wenn der read-Thread den Befehl nameList: erhaelt wird diese in einem String gespeichert und das
	 * Nutzerfeld wird geloescht.
	 * Die Nutzer sind Mittels ":" voneinander getrennt und werden mit split in einem String Array gespeichert
	 * wo sie im Anschluss getrimmt und der Liste hinzugefuegt werden.
	 * 
	 * @param String s : String mit den gespeicherten Nutzernamen.
	 * @param String[] sa: String Array mit dem Nutzernamen nach dem Split.
	 * 
	 * @throws IOException
	 */
	
	@Override
	public void printNameList(String s) throws IOException {
		listeDefault.clear();

		String[] sa = s.split(":");
		for (int i = 0; i < sa.length; i++) {
			sa[i].trim();
			listeDefault.addElement(sa[i].replaceAll(" ", "").replaceAll("nameList", "Online-User:"));
			
		}
	}

	/**
	 * Methode fuer das vesenden von Standard Chat Nachrichten.
	 * Beim Aufrufen dieser Methode wird eine neue Message erstellt und der Befehl message:
	 * davor gesetzt, damit der Nutzer ohne die Angabe des Befehls Nachrichten verschicken kann.
	 * 
	 * @throws IOException
	 */
	
	@Override
	public void printMessage() throws IOException {
			
		Message msg=new Message("message:"+feld_schreiben.getText());			//Der Befehl message: wird vor jede Nachricht gesetzt
		 
		
		trans.writeOut(msg);
		
		printTextIncoming("Du: " + feld_schreiben.getText() + "\n");			//Beim Aufruf der Methode wird der Text im Eingabefeld in das Feld fuer die eingehenden Nachrichten gesetzt
		feld_lesen.setCaretPosition(feld_lesen.getDocument().getLength());		//Damit die Liste automatisch beim Senden den neuste Nachricht anzeigt (Automatisch nach unten scrollen)
		
		feld_schreiben.setText(null);											//Nachricht wird nach dem Senden aus dem Eingabefeld geloescht
		pressed = false;
		
		
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
		
	}
	
	/**
	 * Methode damit beim schliessen des Fensters eine disconnect: Message gesendet wird.
	 * Das ist wichtig damit die Nutzerliste aktuell bleibt.
	 * 
	 */
	
	@Override
	public void windowClosing(WindowEvent e) {
		
		Message msg = new Message("disconnect:");
		try {
			trans.writeOut(msg);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		
	}
	
	public void printTextIncoming(String txt) {
		feld_lesen.append(txt);
	}
	
	public void clearList() {
		listeDefault.clear();
	}
}

