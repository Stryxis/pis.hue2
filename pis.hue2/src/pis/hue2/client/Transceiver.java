package pis.hue2.client;

import java.io.IOException;
import pis.hue2.common.Message;

public interface Transceiver {
	
	public Message readInServer() throws IOException;
	public Message readInUser() throws IOException;
	public void writeOut(Message msg) throws IOException;
}
