package pis.hue2.server;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextArea;

import java.io.*;
import pis.hue2.common.*;

/**
 * Objekte der Klasse TeilnehmerListe stellen eine Liste dar, in der die Teilnehmer eines Chats verwaltet werden.
 * <br>
 * Die TeilnehmerListe kann leer sein und wird mit Teilnehmer gefuellt.
 * <br><br>
 * Klasseninvariante: <br>
 * <ul>
 * <li> ArrayList list: besteht nur aus eingetragenen Teilnehmer, sie kann auch leer sein</li>
 * <li> int maxClients: besteht aus der maximalen Anzahl von Teilnehmer die in dieser Liste gespeichert werden sollen. </li>
 * <li> JTextArea feld_lesen: ist eine Referenz auf die TextArea des Servers </li>
 * </ul>
 * <br><br>
 * 
 * 
 * @author Patrick Schmidt
 * @author Anton Sedelnikow
 *
 */

public class TeilnehmerListe {

	private List <Teilnehmer> list;
	private final int maxClients;
	private Output output;
	
	/**
	 * Initialisiert eine Teilnehmerliste mit einer gewissen Groesse bzw. mit einer Obergrenze an Benutzern
	 * @param maxClients beinhaltet die maximale Anzahl von bedienbaren Benutzern
	 */
	
	public TeilnehmerListe(int maxClients, Output output) {							// Festesetzen der maximalen Clientenanzahl
		this.maxClients = maxClients;
		this.output = output;
		list = new ArrayList<Teilnehmer>();
	}
	
	/**
	 * Fügt einen Teilnehmer zur Liste hinzu.
	 * @param t ist ein Teilnehmer der zur Liste hinzugefuegt werden soll 
	 * @throws TooManyUsersException wenn maxClients ueberschritten wird
	 * @throws SameUserException wenn der uebergebene Teilnehmer gleich wie einer in der Liste ist
	 * 
	 * @pre Liste ist kleiner als die maximale Anzahl von Benutzern
	 * <br> Teilnehmer befindet sich noch nicht in der Liste
	 * @post Liste ist kleiner oder gleich der maximalen Anzahl von Benutzern
	 * <br>Jeder Teilnehmer bzw. jeder Name befindet sich nur einmal in der Liste
	 */
	
	public synchronized void addTeilnehmer(Teilnehmer t) throws TooManyUsersException,SameUserException {				// einen Teilnehmer hinzufügen zur Liste; wichtig: Threadsicher
		if(list.size() <= maxClients - 1) {									// Kontrolle ob die Liste nicht schon zu viele Clients beinhaltet
			list.add(t);
			for (int i = 0; i < list.size(); i++) { 					
				if((list.get(i).getName().equals(t.getName()) && list.get(i).getPort() != t.getPort()) || (list.get(i).getName().equals(t.getName()) == false && list.get(i).getPort() == t.getPort())) { 						
					list.remove(t); 						
					SameUserException ex = new SameUserException("Gleicher Benutzer!"); 						
					throw ex; 					
				} 				
			}
		} else { 					
			TooManyUsersException ex = new TooManyUsersException("Es gibt bereits zu viele Teilnehmer!"); 					
			throw ex; 				
		}
	}
	
	/**
	 * Ein Teilnehmer soll aus der Liste entfernt werden.
	 * @param t ist ein Teilnehmer der aus der Liste entfernt werden soll
	 * 
	 * @pre t befindet sich in der Liste
	 * @post der richtige Teilnehmer t wurde entfernt
	 */
	
	public synchronized void deleteTeilnehmer(Teilnehmer t) {			// löschen des übergebenen Teilnehmers um Platz für neue Clients zu machen
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).equals(t)) {
				list.remove(i);
			}
		}
	}
	
	/**
	 * Gibt eine die Namen aller in der Liste befindlichen Namen in einer Zeichenkette zurueck
	 * @return output als Zeichenkette aller Namen die durch ein : getrennt sind
	 */
	
	@Override
	public String toString() {										//Ausgeben der Namensliste in einem String
		
		String output = "";	
		
		for(int i = 0; i < list.size(); i++) {
			if(i < list.size()) {
				output = output + (list.get(i)).getName() + ":";
			} else {
				output = output + (list.get(i)).getName();
			}
		}
		return output.trim();
	}
	
	/**
	 * Die Namen der Teilnehmerliste werden an alle in der Liste regestrierten Teilnehmer ausgegeben
	 * @throws IOException wenn ein Stream unvorhergesehen gestoppt wird
	 */
	
	public synchronized void sendNames() throws IOException {
		
		for (int i = 0; i < list.size(); i++) {														// Liste wird iteriert um dann bei jedem Teilnehmer
			(list.get(i)).getRef().getPW().println("nameList:" + this.toString().trim());		// den PrintWriter anzusprechen und dort die Namensliste auszugeben
			output.printText("An " + list.get(i).getName() + ": nameList: " + this.toString() + '\n');
		}																							
	}
	
	/**
	 * Eine übergebene Nachricht wird mit dem Namen des Senders an alle anderen Teilnehmer geschickt.
	 * @param msg ist die uebergebene Nachricht welche versendet werden soll
	 * @param name ist der Name desjenigen der die Nachricht gesendet hat
	 * @throws IOException wenn ein Stream unvorhergesehen gestoppt wird
	 */
	
	public synchronized void sendToAll(Message msg, String name) throws IOException {
		
		String help;																		// Hilfsstring um die Nachricht zu formatieren
		
		help = msg.getType().toString() + ": " + name + ": " + msg.getContent(); 			// Nachricht mit Name des Senders und Inhalt
		
		for (int i = 0; i < list.size(); i++) {												// Iteration wie vorher
			if (name.equals((list.get(i)).getName().trim()) == false) {						// Unterscheidung ob der Name des Teilnehmers mit dem
				(list.get(i)).getRef().getPW().println(help.trim());					// ansonsten gibt jeder PrintWriter der Teilnehmer die Nachricht aus.
				output.printText("An " + list.get(i).getName() + ": " + help.toString() + '\n');
			}																				
		}
		
	}
}
