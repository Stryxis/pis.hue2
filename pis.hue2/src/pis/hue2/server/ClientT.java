package pis.hue2.server;

import java.net.*;

import javax.swing.JTextArea;

import java.io.*;
import pis.hue2.common.*;

/**
 * Objekte der Klasse ClientT stellen eine ein Runnable Objekt dar, mit dem ein Thread initialisiert werden kann der die 
 * Benutzer an einem Server bedient.
 * <br>
 * Mit ClientT initialisierte Threads arbeiten mit Message Objekten und vergleichen eingelesene Nachrichten mit den jewweiligen in 
 * der Klasse Message definierten Typen.
 * <br>
 * Dabei werden je nach Typ der Nachricht verschiedenen Aktionen durchgefuehrt.
 * <br><br>
 * Klasseninvariante: <br>
 * <ul>
 * <li> Socket client: ist der Socket auf dem die Verbindung mit dem jeweiligen Benutzer initialisiert wurde</li>
 * <li> BufferedReader inFromClient: ist immer der jeweilige InputStream des Benutzers</li>
 * <li> PrintWriter outToClient: ist immer der jeweilige OutputStream des Benutzers</li>
 * <li> Message in: ist immer die Nachricht die vom Clienten verschickt werden soll </li>
 * <li> Message out: ist immer die Nachricht, die vom Server zu den Benutzern gesendet wird </li>
 * <li> Message invalid: wird nur gebraucht wenn Exceptions geworfen werden </li>
 * <li> Teilnehmer member: ist der Teilnehmer der von diesem Runnable Objekt bedient wird </li>
 * <li> TeilnehmerListe chatter: ist die eine Teilnehmerliste in der alle vom Sever bedienten Benutzer gespeichert werden </li>
 * <li> boolena open: ist die Variable fuer die Endlosschleifen, wird nur dann false wenn sich ein Client mit disconnect abmeldet </li>
 * <li> JTextArea feld_lesen: ist die Referenz auf die TextArea des Servers </li>
 * </ul>
 * <br><br>
 * 
 * 
 * @author Patrick Schmidt
 * @author Anton Sedelnikow
 *
 * @see Message
 * @see Teilnehmer
 * @see TeilnehmerListe
 */

public class ClientT implements Runnable {
	
	private Socket client;
	private BufferedReader inFromClient;
	private PrintWriter outToClient;
	private Message in;
	private Message out;
	private Message invalid;
	private Teilnehmer member;
	private static TeilnehmerListe chatter;
	private boolean open;
	private Output output;
	
	/**
	 * Initialisiert ein Runnable-Objekt um einen Benutzer zu bedienen.
	 * @param client ist der jeweilige Socket ueber den der Benutzer bedient wird
	 * @param chatter ist die allgemeingueltige einzige Teilnehmerliste des Servers
	 * @param feld_lesen ist die Referenz auf die TextArea des Servers
	 * @throws IOException wenn der Stream unvorhergesehen unterbrochen wird
	 */
	
	ClientT(Socket client, TeilnehmerListe chatter, Output output) throws IOException {
		this.client = client;
		ClientT.chatter = chatter;	
		inFromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
		outToClient = new PrintWriter(client.getOutputStream(), true);
		this.output = output;
	}
	
	/**
	 * In einer Endlosschleife wird ein String eingelsen und als Message-Objekt initialisiert. Es wird dann nach dem Typ der eingelesenen
	 * Nachricht verglichen. Je nach Typ der Nachricht werden andere Aktionen durchgefuehrt.
	 * <br>
	 * Zudem wird bei jeder eintreffenden Nachricht diese auch im TextArea des Servers ausgegeben.
	 * 
	 * @throws IOException wenn die Streams unvorhergesehen unterbrochen werden
	 * @throws IllegalUserException wenn es nicht moeglich ist bei einer connect-Message einen Teilnehmer zu initialisieren
	 * @throws TooManyUsersException wenn die maximale Benutzeranzahl in der Teilnehmerliste ueberschritten wird
	 * @throws SameUserException wenn der initialisierte Teilnehmer schon angemeldet ist oder ein anderer Benutzer den gleichen Namen
	 * benutzen will
	 * 
	 * @see Message
	 * @see Teilnehmer
	 */
	
	@Override
	public void run() {
		
		open = true;
		String inLine;
		while(open) {
			try {
				while((inLine = inFromClient.readLine()) != null) {
					in = new Message(inLine);						// einlesen vom Clienten
					switch (in.getType()) {							// ermitteln des Typ der jeweiligen Nachrich --> Enum in Nachrichten Klasse
					case connect:
						member = new Teilnehmer(in.getContent().toString().trim(), client.getInetAddress(), client.getPort(), this);		//bei der connect Nachricht wird ein Teilnehmer erschaffen
						chatter.addTeilnehmer(member);								// Teilnehmer wird in der Liste gespeichert
						out = new Message("connectConfirm:OK");
						outToClient.println(out.toString().trim());						// Ausgabe zum Client
						chatter.sendNames();
						output.printText("Von " + member.getName() + ": " + in.toString() + '\n');
						output.printText("An " + member.getName() + ": " + out.toString() + '\n');
						break;
					case disconnect:												// Schließung aller Streams + Socket
						chatter.deleteTeilnehmer(member);
						chatter.sendNames();
						out = new Message("disconnectConfirm:OK");
						outToClient.println(out.toString().trim());
						closeClient();
						output.printText("Von " + member.getName() + ": " + in.toString() + '\n');
						output.printText("An " + member.getName() + ": " + out.toString() + '\n');
						Thread.currentThread().interrupt();
						open = false;
						break;
					case message:
						output.printText("Von: " + member.getName() + ": " + in.toString() + '\n');
						chatter.sendToAll(in, member.getName());					// threadsichere Methode zum Senden der Namen an alle Clients
						break;														// wobei ich da noch keine wirkliche Ahnung habe
					case nameList:													// genau wie hier, noch keine Ahnung wie man dem Server sagt
						chatter.sendNames();										// sende es an alle angemeldeten
						output.printText("Von " + member.getName() + ": " + in.toString() + '\n');
						break;
					default:
						outToClient.println("Fehler!");
						break;
					}
				}
			} catch (IOException e) {
				System.out.println(e);
				Thread.currentThread().interrupt();
			} catch (IllegalUserException ex) { 				
				invalid = new Message("refused:invalid_name"); 				
				outToClient.println(invalid.toString().trim()); 
				output.printText("An " + member.getName() + ": " + invalid.toString() + '\n');
				try { 					
					out = new Message("disconnectConfirm:"); 
					output.printText("An " + member.getName() + ": " + out.toString() + '\n');
					outToClient.println(out.toString()); 					
					closeClient();
					Thread.currentThread().interrupt();
				} catch (IOException e) { 					
						e.printStackTrace(); 				
					}
			} catch (TooManyUsersException ex) { 				
				invalid = new Message("refused:too_many_Users"); 				
				outToClient.println(invalid.toString().trim()); 
				output.printText("An " + member.getName() + ": " + invalid.toString() + '\n');
				try { 					
					out = new Message("disconnectConfirm:");
					output.printText("An " + member.getName() + ": " + out.toString() + '\n');
					outToClient.println(out.toString()); 					
					closeClient();
					Thread.currentThread().interrupt();
				} catch (IOException e) { 					
				e.printStackTrace(); 				
				}
			} catch (SameUserException ex) { 				
				invalid = new Message("refused:name_in_use"); 				
				outToClient.println(invalid.toString().trim());
				output.printText("An " + member.getName() + ": " + invalid.toString() + '\n');
				try { 					
					out = new Message("disconnectConfirm:");
					output.printText("An " + member.getName() + ": " + out.toString() + '\n');
					outToClient.println(out.toString()); 					
					closeClient();
					Thread.currentThread().interrupt();
				} catch (IOException e) { 					
					e.printStackTrace(); 				
				} 			
			}
		}
	}
	
	/**
	 * Gibt eine Referenz auf den OutputStream des Clienten zurueck
	 * @return outToClient als PrintWriter
	 */
	
	public PrintWriter getPW() {										// Methode um eine Referenz zum PrintWriter um damit etwas zu schicken
		return outToClient;
	}
	
	private void closeClient() throws IOException {
		inFromClient.close();
		outToClient.close();
		client.close();
	}
	
}
