package pis.hue2.server;

import javax.swing.SwingUtilities;

/**
 * Klasse zum Starten des Servers
 * 
 * @author Patrick Schmidt
 * @author Anton Sedelnikow
 *
 */

public class LaunchServer {
	
	public static void main(String[] args) {
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Output gui = new ServerGUI();
			}
		});
	}
}
