package pis.hue2.server;

import javax.swing.*;

import javax.swing.border.EmptyBorder;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.ServerSocket;

/**
 * Klasse um die grafische Oberflaeche zu erstellen.
 * <br>
 *  Es werden Labels, TextAreas und Buttons angelegt,
 * welche im Anschluss initialisiert werden.
 * Um die Funktionalitaet zu erhoehen wurden ActionListener  angelegt, welche beim klicken auf den Button reagieren
 * <br>
 * 
 * <ul>
 * <li>ServerSocket serverSocket: ServerSocket mit Port</li>
 * <li>int PORT: Der gesetzte Port</li>
 * 
 * 
 * </ul>
 * 
 * 
 * @author Patrick Schmidt
 * @author Anton Sedelnikow
 *
 */

public class ServerGUI extends JFrame implements Output, ActionListener {
	
	private static ServerSocket serverSocket;
	final static int PORT = 12121;
	private static AcceptT accept;
	private static Thread connect;

	// Frame
	JFrame frame = new JFrame("Server");

	private JTextArea feld_lesen;

	// ScrollPanel
	private JScrollPane scroll_lesen;

	// Buttons
	private JButton server_starten;
	private JButton server_stoppen;

	private JPanel container_buttons = new JPanel();

	private JPanel container_top = new JPanel();

	/**
	 * Konstruktor indem Titel, Groesse und die Standard Methode zum schliessen definiert wird.
	 * Anschliessend werden die Methoden zur Initialisierung der Komponenten und Listener aufgerufen. 
	 * 
	 */
	
	public ServerGUI() {
		
		setTitle("Server");
		setSize(600, 200);

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		this.initComp();
		this.initListen();

		setVisible(true);
	}
	
	/**
	 *Konstruktor indem Titel, Groesse und die Standard Methode zum schliessen definiert wird.
	 *Anschliessend werden die Methoden zur Initialisierung der Komponenten und Listener aufgerufen.
	 *
	 *Buttons wurden in einem Container zusammengefasst und dem Page_Start Container des Layouts hinzugefügt.
	 *Den Center Container des Layouts nimmt die Textarea ein inder die Serverlogs angezeigt werden.
	 */
	
	private void initComp() {

		// Textfields
		// ----------------------------------
		this.feld_lesen = new JTextArea();
		feld_lesen.setEditable(false);

		// ----------------------------------

		// ScrollPane
		// ----------------------------------
		this.scroll_lesen = new JScrollPane(feld_lesen);

		// ----------------------------------

		// Buttons
		// ----------------------------------
		this.server_starten = new JButton("Starten");
		this.server_stoppen = new JButton("Stoppen");
		// ----------------------------------

		// Buttons

		this.container_buttons.add(server_starten);
		this.container_buttons.add(server_stoppen);

		this.container_top.add(container_buttons);
		// ------------------------------------

		// Frame
		this.getContentPane().add(this.container_top, "North");

		this.getContentPane().add(this.scroll_lesen, "Center");

	}

	/**
	 * Methode zur Initialisierung der Listener
	 * 
	 * Starten und Stoppen Button wurden mit ActionCommands und -Listenern versehen
	 * um auf Knopfdruck zu reagieren.
	 * 
	 */
	
	private void initListen() {
		this.server_starten.setActionCommand("starten");
		this.server_starten.addActionListener(this);

		this.server_stoppen.setActionCommand("stoppen");
		this.server_stoppen.addActionListener(this);
	}

	/**
	 * Methoden des ActionListeners
	 * 
	 * Beim betätigen des Start Buttons
	 * wird ein neuer ServerSocket mit Port erzeugt. Anschließend wird 
	 * der Thread zum Annehmen eines Clienten initialisiert und gestartet.
	 * 
	 * Beim Betätigen des stoppen Buttons wird der Server geschlossen.
	 * 
	 * 
	 */
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		if(command.equals("starten")){
			
			try {
				int port = PORT;
				
				serverSocket = new ServerSocket(port);					//selbst gesetzter erster Port
				accept = new AcceptT(serverSocket, this);			// der Thread zum Annehmen eines Clienten wird initialisiert.
				connect = new Thread(accept);
				connect.start();
				printText("---Server gestartet---\n");
			} catch (IOException ex) {
				System.err.println(ex);
			}
			
		}else if(command.equals("stoppen")){
			try {
				printText("---Server gestoppt---\n");
				connect.interrupt();
				serverSocket.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}

	}

	@Override
	public synchronized void printText(String txt) {
		feld_lesen.append(txt);
		
	}
	
}

