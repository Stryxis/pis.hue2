package pis.hue2.server;

import java.io.*;
import java.net.*;

import javax.swing.JTextArea;

/**
 * Objekte der Klasse AcceptT stellen ein Runnable-Objekt dar, mit dem ein Thread erzeugt werden kann, der fuer einen Server
 * in einer Endlosschleife verschiedene Clienten annimmt und für diese jeweils einen neuen Thread erzeugt um sie zu bedienen.
 * <br>
 * AcceptT-Objekte besitzen immer einen ServerSocket und einen Socket zum Annehmen von Clients. Sie initialisieren weitere
 * Threads für die Clienten und bedienen diese so.
 * <br><br>
 * Klasseninvariante: <br>
 * <ul>
 * <li> ServerSocke server: besteht aus einem Port fuer den Server, er darf nicht null sein.</li>
 * <li> Socket welcomeSocket: besteht aus einem Socket auf dem die Clienten angenommen werden.</li>
 * <li> TeilnehmerListe chatter: besteht aus einer Liste um die Clienten und ihre Daten zu speichern, sie darf auch leer sein.</li>
 * <li> int maxClients: besteht immer aus einer ganzen Zahl, die die Anzahl der maximal für den Server zugelassenen Clienten bestimmt.</li>
 * <li> JTextArea feld_lesen: ist eine Referenz auf die TextArea der grafischen Oberflaeche </li>
 * </ul>
 * <br><br>
 * 
 * 
 * @author Patrick Schmidt
 * @author Anton Sedelnikow
 *
 */

public class AcceptT implements Runnable {

	private ServerSocket server;
	private Socket welcomeSocket;
	private TeilnehmerListe chatter;
	final int maxClients = 3;
	private Output output;

	/**
	 * Initialisiert ein AcceptT, welches Runnable implementiert.
	 * @param server beinhaltet den Socket des Servers, darf nicht null sein.
	 * @param feld_lesen ist die Referenz auf die TextArea von der ServerGUI
	 */
	
	public AcceptT(ServerSocket server, Output output) {
		this.server = server;
		this.output = output;
	}
	
	/**
	 * Initialisierung der allgemeingueltigen TeilnehmerListe sowie das Annehmen der Clienten und das Initialisieren und starten der Threads
	 * fuer die jeweiligen Clienten. 
	 */
	
	@Override
	
	public void run() {

		chatter = new TeilnehmerListe(maxClients, output);				// initialisierung der allgemeingültigen Liste													// alle wichtigen Daten werden über die Teilnehmer-Klasse hier gespeichert.
		
		while (true) {
			try {
				if(server.isClosed() == false) {
					
					welcomeSocket = server.accept();						// Verbindungsaufbau mit einem Client

						ClientT cT = new ClientT(welcomeSocket, chatter, output);			// Thread für die Bedienung der Clients wird initialisiert
						Thread client = new Thread(cT);
						client.start();
				} else {
					
				}
			}
			catch (IOException e) {
					System.out.println(e);
					Thread.currentThread().interrupt();
			}
		}
	
	}
}